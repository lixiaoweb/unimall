package com.iotechn.unimall.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnimallCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnimallCoreApplication.class, args);
	}

}
